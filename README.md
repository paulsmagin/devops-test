# DevOps Test

This a simple k8s cluster deployment using Gitlab CI, Docker and Helm. 
All steps tested in local minikube cluster.

#Points of progress

__1.Create a deployment (without helm)__
```
$ kubectl apply -f chart/templates/deployments.yaml
```
Result
```
➜  devops-test git:(main) ✗ kubectl get deployments.apps                    
NAME                   READY   UP-TO-DATE   AVAILABLE   AGE
helloapp-deployment    3/3     3            3           35m
```
```
➜  devops-test git:(main) ✗ kubectl get pods 
helloapp                                1/1     Running   0          36m
helloapp-deployment-54d5b67d8c-j2jqw    1/1     Running   0          36m
helloapp-deployment-54d5b67d8c-k7858    1/1     Running   0          36m
helloapp-deployment-54d5b67d8c-wgcdc    1/1     Running   0          36m
```

__2. Local DNS name__

Recognize address to match it with dns name
```
➜  devops-test git:(main) minikube service web --url

```
Output will be
```
http://ip_addr:port
```
Then check info of ingress
```
➜  devops-test git:(main) ✗ kubectl get ingress
NAME              CLASS   HOSTS                     ADDRESS     PORTS   AGE
helloapp          nginx   hello-world.example.com   <ip_addr>   80      39m
```

To run with specific dns-name locally, add following line to ```/etc/hosts```
```
<ip_addr>  hello-world.example.com
```
To test this, try
```
➜  devops-test git:(main) curl hello-world.example.com
```
outputs
```
Hello, world!
```

__3. Using Helm__

To run deployment, try
```
➜  devops-test git:(main) ✗ helm install helloapp chart/
NAME: helloapp
LAST DEPLOYED: Wed Feb 22 17:43:55 2022
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
```