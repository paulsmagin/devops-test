FROM golang:latest 

WORKDIR /helloapp 

COPY main.go /helloapp
COPY go.mod /helloapp
COPY go.sum /helloapp

RUN go build -o main .

EXPOSE 9000
CMD ["/helloapp/main"]
